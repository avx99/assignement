package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Date;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Utilisateur;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class UtilisateurRepositoryTest {
	
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Test
	@Order(1)
	public void testCreate() {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("user1");
		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");
		utilisateur1.setBirthdate(new Date());
		
		utilisateur1 = utilisateurRepository.save(utilisateur1);
		
		assertNotNull(utilisateur1);
	}
	
	@Test
	@Order(2)
	public void testGetOne() {
		assertEquals("user1", utilisateurRepository.findAll().get(0).getUsername());
	}

	@Test
	@Order(3)
	public void testGetAll() {
		assertEquals(1, utilisateurRepository.findAll().size());
		
	}
	
	@Test
	@Order(4)
	public void testUpdate() {
		Utilisateur utilisateur1 = utilisateurRepository.findByUsername("user1");
		utilisateur1.setUsername("userx");
		
		utilisateurRepository.save(utilisateur1);
		
		assertEquals("userx",utilisateurRepository.findByUsername("userx").getUsername());
	}
	
	@Test
	@Order(5)
	public void testDelete() {
		Utilisateur u = utilisateurRepository.findByUsername("userx");
		utilisateurRepository.deleteById(u.getId());
		assertThat(utilisateurRepository.existsById(u.getId())).isFalse();
	}
	
}
