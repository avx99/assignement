package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Versement;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class VersementRepositoryTest {
	
	@Autowired
	private VersementRepository versementRepository; 
	@Test
	@Order(1)
	public void testCreate() {
		Versement versement = new Versement();
		versement.setMontantVirement(BigDecimal.TEN);
		versement.setMotifVersement("motif1");
		versement.setNom_prenom_emetteur("ouss");
		
		versement = versementRepository.save(versement);
		
		assertNotNull(versement);
	}
	
	@Test
	@Order(2)
	public void testGetOne() {
		assertEquals("motif1", versementRepository.findAll().get(0).getMotifVersement());
	}

	@Test
	@Order(3)
	public void testGetAll() {
		assertEquals(1, versementRepository.findAll().size());
		
	}
	
	@Test
	@Order(4)
	public void testUpdate() {
		Versement versement = versementRepository.findByMotifVersement("motif1");
		versement.setMotifVersement("motifx");
		
		versementRepository.save(versement);
		
		assertEquals("motifx",versementRepository.findByMotifVersement("motifx").getMotifVersement());
	}
	
	@Test
	@Order(5)
	public void testDelete() {
		Versement versement = versementRepository.findByMotifVersement("motifx");
		versementRepository.deleteById(versement.getId());
		assertThat(versementRepository.existsById(versement.getId())).isFalse();
	}

}
