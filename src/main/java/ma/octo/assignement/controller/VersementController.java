package ma.octo.assignement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.service.VersementService;

@RestController
@RequestMapping(value = "/api/versement")
public class VersementController {
	@Autowired
	private VersementService versementService;
	
	@GetMapping
	public List<Versement> getAllVersement() {
		return versementService.getAllVersement();
	}
	
	@GetMapping("{id}")
	public Versement getVersement(@PathVariable("id") Long id) {
		return versementService.getVersement(id);
	}
	
	@PostMapping
	public Versement createVersement(@RequestBody Versement versement) {
		return versementService.createVersement(versement);
	}
	
	@PutMapping("{id}")
	public Versement updateVersement(@RequestBody Versement versement,@PathVariable("id") Long id) {
		return versementService.updateVersement(versement, id);
	}
	
	@DeleteMapping("{id}")
	public Versement deleteVersement(@PathVariable("id") Long id) {
		return versementService.deleteVersement(id);
	}
}
