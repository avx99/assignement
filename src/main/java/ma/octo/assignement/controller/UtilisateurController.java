package ma.octo.assignement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;

@RestController
@RequestMapping(value = "/api/utilisateur")
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService utilisateurService;
	
	@GetMapping
	public List<Utilisateur> getAllUtilisateur(){
		return utilisateurService.getAllUtilisateur();
	}
	
	@GetMapping("{id}")
	public Utilisateur getUtilisateur(@PathVariable("id") Long id){
		return utilisateurService.getUtilisateur(id);
	}
	
	@PostMapping
	public Utilisateur createUtilisateur(@RequestBody Utilisateur utilisateur){
		return utilisateurService.createUtilisateur(utilisateur);
	}
	
	@PutMapping("{id}")
	public Utilisateur updateUtilisateur(@RequestBody Utilisateur utilisateur,@PathVariable("id") Long id){
		return utilisateurService.updateUtilisateur(utilisateur,id);
	}
	
	@DeleteMapping("{id}")
	public Utilisateur deleteUtilisateur(@PathVariable("id") long id){
		return utilisateurService.deleteUtilisateur(id);
	}
	

}
