package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Utilisateur;

public interface UtilisateurService {
	List<Utilisateur> getAllUtilisateur();
	Utilisateur getUtilisateur(Long id);
	Utilisateur createUtilisateur(Utilisateur utilisateur);
	Utilisateur updateUtilisateur(Utilisateur utilisateur, Long id);
	Utilisateur deleteUtilisateur(Long id);
}
