package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Versement;

public interface VersementService {
	List<Versement> getAllVersement();
	Versement getVersement(Long id);
	Versement createVersement(Versement versement);
	Versement updateVersement(Versement versement, Long id);
	Versement deleteVersement(Long id);
}
