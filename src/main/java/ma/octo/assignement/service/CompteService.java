package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Compte;

public interface CompteService {
	List<Compte> getAllCompte();
	Compte getCompte(Long id);	
	Compte createCompte(Compte compte);
	Compte updateCompte(Compte compte, Long id);
	Compte deleteCompte(Long id);
}
