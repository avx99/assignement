package ma.octo.assignement.service.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.ResourceNotFoundException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.CompteService;

@Service
@Transactional
public class CompteServiceImpl implements CompteService{
	
	@Autowired
	private CompteRepository compteRepository;
	
	@Override
	public List<Compte> getAllCompte() {
		return compteRepository.findAll();
	}

	@Override
	public Compte getCompte(Long id) {
		return compteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Compte", "id", id));
	}

	@Override
	public Compte createCompte(Compte compte) {
		return compteRepository.save(compte);
	}

	@Override
	public Compte updateCompte(Compte compte, Long id) {
		Compte oldCompte = compteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Compte", "id", id));
		oldCompte.setNrCompte(compte.getNrCompte());
		oldCompte.setRib(compte.getRib());
		oldCompte.setSolde(compte.getSolde());
		oldCompte.setUtilisateur(compte.getUtilisateur());
		return compteRepository.save(oldCompte);
	}

	@Override
	public Compte deleteCompte(Long id) {
		Compte compte = compteRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Compte", "id", id));
		compteRepository.deleteById(id);
		return compte;
	}

}
