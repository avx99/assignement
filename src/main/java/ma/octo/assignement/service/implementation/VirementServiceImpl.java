package ma.octo.assignement.service.implementation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.ResourceNotFoundException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.VirementService;

@Service
@Transactional
public class VirementServiceImpl implements VirementService{

	public static final int MONTANT_MAXIMAL = 10000;
	@Autowired
	private VirementRepository virementRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private AuditService auditService;
	@Override
	
	public List<Virement> getAllVirement() {
		return virementRepository.findAll();
	}

	@Override
	public Virement getVirement(Long id) {
		return virementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Virement", "id", id));
	}

	@Override
	public Virement createVirement(Virement virement)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException 
	{
	        if (virement.getCompteEmetteur() == null) {
	            throw new CompteNonExistantException("Compte emetteur Non existant");
	        }
	        if (virement.getCompteBeneficiaire() == null) {
	            throw new CompteNonExistantException("Compte beneficiere Non existant");
	        }
	        
	        if (virement.getMontantVirement().equals(null) || virement.getMontantVirement().intValue() == 0) {
	            throw new TransactionException("Montant vide");
	        } else if (virement.getMontantVirement().intValue() < 10) {
	            throw new TransactionException("Montant minimal de virement non atteint");
	        } else if (virement.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
	            throw new TransactionException("Montant maximal de virement dépassé");
	        }

	        
	        if (virement.getMotifVirement().equals(null) ||  virement.getMotifVirement().length() == 0) {
	            throw new TransactionException("Motif vide");
	        }
	        
	        if (virement.getCompteEmetteur().getSolde() == null) {
	            throw new SoldeDisponibleInsuffisantException("Solde du compte emetteur Non existant");
	        }
	        if (virement.getCompteBeneficiaire().getSolde() == null) {
	            throw new SoldeDisponibleInsuffisantException("Solde du compte beneficiere Non existant");
	        }
	        if (virement.getCompteEmetteur().getSolde().intValue() - virement.getMontantVirement().intValue() < 0) {
	        	throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l\'utilisateur");
	        }

	        virement.getCompteEmetteur().setSolde(virement.getCompteEmetteur().getSolde().subtract(virement.getMontantVirement()));
	        compteRepository.save(virement.getCompteEmetteur());

	        virement.getCompteBeneficiaire().setSolde(virement.getCompteBeneficiaire().getSolde().add(virement.getMontantVirement()));
	        compteRepository.save(virement.getCompteBeneficiaire());

	        Virement v = new Virement();
	        v.setDateExecution(virement.getDateExecution());
	        v.setCompteBeneficiaire(virement.getCompteBeneficiaire());
	        v.setCompteEmetteur(virement.getCompteEmetteur());
	        v.setMontantVirement(virement.getMontantVirement());

	        virementRepository.save(virement);
	        
	        Audit audit = new Audit();
	        audit.setMessage("Virement depuis " + virement.getCompteEmetteur().getNrCompte() + " vers " + virement.getCompteBeneficiaire().getNrCompte() +
	        		" d\'un montant de " + virement.getMontantVirement().toString());
	        audit.setEventType(EventType.VIREMENT);
	        auditService.createAudit(audit);
	        return virement;
	}

	@Override
	public Virement updateVirement(Virement virement, Long id) {
		Virement oldVirement = virementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Virement", "id", id));;
		oldVirement.setCompteBeneficiaire(virement.getCompteBeneficiaire());
		oldVirement.setCompteEmetteur(virement.getCompteEmetteur());
		oldVirement.setDateExecution(virement.getDateExecution());
		oldVirement.setMontantVirement(virement.getMontantVirement());
		oldVirement.setMotifVirement(virement.getMotifVirement());
		return oldVirement;
	}

	@Override
	public Virement deleteVirement(Long id) {
		Virement oldVirement = virementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Virement", "id", id));
		virementRepository.deleteById(id);
		return oldVirement;
	}

	/*
	@Override
	public Virement createTransaction(VirementDto virementDto)
			throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null || compteBeneficiaire == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (virementDto.getMontantVirement().equals(null) || virementDto.getMontantVirement().intValue() == 0) {
            throw new TransactionException("Montant vide");
        } else if (virementDto.getMontantVirement().intValue() < 10) {
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (virementDto.getMontantVirement().intValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().length() < 0) {
            throw new TransactionException("Motif vide");
        }
        
        if (compteEmetteur.getSolde().intValue() - virementDto.getMontantVirement().intValue() < 0) {
        	throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l\'utilisateur");
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(virementDto.getMontantVirement()));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + virementDto.getMontantVirement().intValue()));
        compteRepository.save(compteBeneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontantVirement(virementDto.getMontantVirement());

        virementRepository.save(virement);
        
        Audit audit = new Audit();
        audit.setMessage("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d\'un montant de " + virementDto.getMontantVirement()
                .toString());
        audit.setEventType(EventType.VIREMENT);
        auditService.createAudit(audit);
        return virement;
	}*/

}
